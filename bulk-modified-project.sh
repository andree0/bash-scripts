#!/bin/bash -e
PREFIX_DIR=~/WORK/KRS
cd $PREFIX_DIR
DIRS="$(ls -d */)"

NEW_BRANCH="f/gitlab-ci-linter"

# stash changes
for DIR in $DIRS; do
  echo $DIR && cd $PREFIX_DIR/${DIR%/} && ls -a | grep ".git" && git stash || true && cd .. ;
done

# pull default branch
for DIR in $DIRS; do
  echo $DIR && cd $PREFIX_DIR/${DIR%/} && ls -a | grep ".git" \
  && DEFAULT_BRANCH=$(git symbolic-ref refs/remotes/origin/HEAD | sed 's@^refs/remotes/origin/@@') \
  && git checkout $DEFAULT_BRANCH && git fetch && git pull origin $DEFAULT_BRANCH || true && cd .. ;
done

# switch to new branch
for DIR in $DIRS; do
  echo $DIR && cd $PREFIX_DIR/${DIR%/} \
  && ls -a | grep ".pre-commit-config.yaml" && ls -a | grep ".git" \
  && git checkout -b $NEW_BRANCH && echo 'switch new branch' \
  && git status -b \
  && cd .. ;
done

# modify projects
for DIR in $DIRS; do 
  echo $DIR && cd $PREFIX_DIR/${DIR%/} \
  && ls -a | grep ".pre-commit-config.yaml" \
  && echo '
  # required set GITLAB_PRIVATE_TOKEN in shell (your local enviroment)
  # otherwise you get a 404 error
  - repo: https://gitlab.com/devopshq/gitlab-ci-linter
    rev: "v1.0.2"
    hooks:
      - id: gitlab-ci-linter
        args: ["--project=40035410"]  # repo pipelines' >> .pre-commit-config.yaml \
  && ls -a | grep "bitbucket-pipelines.yml" && rm bitbucket-pipelines.yml \
  || true && cd .. ;
done

# push changes
for DIR in $DIRS; do
  echo $DIR && cd $PREFIX_DIR/${DIR%/} && ls -a | grep ".pre-commit-config.yaml" && ls -a | grep ".git" \
  && git status -b | grep $NEW_BRANCH \
  && git add .pre-commit-config.yaml \
  && git commit -m "chore: add gitlab-ci-linter to pre-commit" && git push origin $NEW_BRANCH && echo "changes are pushed" \
  && ls -a | grep "bitbucket-pipelines.yml" \
  && git add bitbucket-pipelines.yml \
  && git commit -m "chore: remove bitbucket-pipelines.yml" && git push origin $NEW_BRANCH && echo "changes are pushed" \
  || true && cd .. ;
done

echo "$?"
